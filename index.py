from collections import Counter
import os, sys
import argparse

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('mainfunction', type=str, help='main function to call')
    parser.add_argument('function', type=str, help='function to call')
    parser.add_argument('-n', '--number', type=int, default=10, help='max number of most accuring words.')
    parser.add_argument('-filename', '--filename', type=str, default='input.txt', help='name of the input file')

    args = parser.parse_args()

    mainfunc = args.mainfunction
    func = args.function

    if  mainfunc != 'run':
        print('Did not find run command, exiting...')
        sys.exit()

    if func == 'count_frequency':
        eval(args.function)(args.number, args.filename)
    elif func == 'count_words':
        eval(args.function)(args.filename)
    else:
        print('Please provide correct function, exiting...')
        sys.exit()

def count_words(filename):
    file = open(filename, 'r')
    data = file.read()
    words = data.split()

    print('Number of words in text file :', len(words))

def count_frequency(times,filename):
    with open(filename) as input_file:
        count = Counter(word for line in input_file
                             for word in line.split())

        for value, count in count.most_common(times):
            print('word name: ' + '"' + str(value) + '"', ' usage : ' + str(count))

if __name__ == '__main__':
    main()
