### Simple python3 app that counts all and specific words from a txt file.

**Usage:**
-  **Exp:** Count all words from input.txt file.
-  **Command** -> python3 index.py run count_words -filename input.txt
-  **Exp:** Count most accuring 3 words from input.txt file.
-  **Command** -> python3 index.py run count_frequency -filename input.txt -n 3
